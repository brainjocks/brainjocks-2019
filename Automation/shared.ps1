function DetectSitecore()
{
    Write-Host "Detecting Sitecore..."

    if (!(Test-Path ".\sandbox\*\sitecore\shell\sitecore.version.xml"))
    {
        throw "No Sitecore detected in the .\sandbox. Please first install Sitecore and then run the setup wizard again"
    }

    $xml = [xml](Get-Content ".\sandbox\*\sitecore\shell\sitecore.version.xml" -Raw)

    $revision = $xml.information.version.revision;

    $majorVerion = "$($xml.information.version.major)"
    $minorVersion = "$($xml.information.version.minor)"

    $sitecore = @{ "Version"="N/A"; "Build"="N/A"; "Revision"="N/A"; "NuGetVersion"="N/A"; "BucketsNuGetVersion"="N/A"; "ContentSearchNuGetVersion"="N/A"; "ContentSearchLinqNuGetVersion"="N/A";
    "ExperienceEditorNuGetVersion"="N/A"; "ExperienceEditorSpeakNuGetVersion"="N/A"; "ExperienceEditorSpeakRibbonNuGetVersion"="N/A"; "MvcNuGetVersion"="N/A"; 
    "MvcExperienceEditorNuGetVersion"="N/A"; "NexusNuGetVersion"="N/A"; "NexusLicensingNuGetVersion"="N/A"; "SpeakClientNuGetVersion"="N/A"; "UpdateNuGetVersion"="N/A"; "Detected"=$False;}
	
    $sitecore.Version = $majorVerion + "." + $minorVersion;

	if(![string]::IsNullOrEmpty($xml.information.version.build)) { # case for new version format like a: 9.1.1
		$sitecore.Build = $xml.information.version.build;
	}
	
    $sitecore.Revision = $xml.information.version.revision;

	$sitecore.NuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.BucketsNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.ContentSearchNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.ContentSearchLinqNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.ExperienceEditorNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.ExperienceEditorSpeakNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.ExperienceEditorSpeakRibbonNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.MvcNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.MvcExperienceEditorNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.NexusNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.NexusLicensingNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision ".\sandbox\*\bin\Sitecore.Nexus.Licensing.dll";
	$sitecore.SpeakClientNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;
	$sitecore.UpdateNuGetVersion = DetectNuGetVersion $sitecore.Version $sitecore.Build $sitecore.Revision;

    $sitecore.Detected = $True;
	
	Write-Host " -- Detected -- Version: $($sitecore.Version) | Build: $($sitecore.Build) | Revision: $($sitecore.Revision) |" -foregroundcolor "yellow"

    Return $sitecore
}

function DetectNuGetVersion($version, $build, $revision, $path = [string]::Empty){
	If ($version -eq 9.1) {
		# Sitecore 9.1 has a very different versnioning. 
		# For most part of Sitecore 9.1.x versions uses a version of the NuGet package equal to the version of Sitecore. However, there is an exception for the Sitecore.Nexus.Licencing.dll.
		if([string]::IsNullOrEmpty($path) -or !(Test-Path $path)) {		
			return "$version.$build";
		}
		# However, there is an exception for the Sitecore.Nexus.Licencing.dll. For the exception cases we are going to use FileVersion.
		(Get-Item $path | Select -first 1 -ExpandProperty VersionInfo).FileVersion -match '^(\d+\.\d+\.\d+)\.\d+$' | Out-Null;
		# return the first group from the regex. that group suppose to be a Sitecore 9.1 nuget package version. hope they follow the same pattern with Sitecore 9.1 update 1 and the rest.
		return $matches[1];
	}
	Else {
	    # that is a default nuget package for Sitecore packages 8.0.x up to the latest 9.0.x
		return "$version.$revision";
	}
}