;(function(global) {

    global.require = global.require || {};

    // paths
    global.require.paths = global.require.paths || {};
    global.require.paths.BrainJocks = "/Areas/BrainJocks/js";

    // shim
    global.require.shim = global.require.shim || {};

    // map
    global.require.map = global.require.map || {};
    global.require.map["*"] = global.require.map["*"] || {};

})(this /* window */);
