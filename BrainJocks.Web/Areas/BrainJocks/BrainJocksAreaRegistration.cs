using System.Web.Mvc;

namespace BrainJocks.Web.Areas.BrainJocks
{
    public class BrainJocksAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BrainJocks";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            // Register your MVC routes in here
        }
    }
}
