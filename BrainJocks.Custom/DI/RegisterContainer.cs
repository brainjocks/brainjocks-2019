﻿using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using BrainJocks.Custom.Services.Salesforce;

namespace BrainJocks.Custom.DI
{
    public class RegisterContainer : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton(typeof(ISalesforceService), typeof(SalesforceService));
        }
    }
}
