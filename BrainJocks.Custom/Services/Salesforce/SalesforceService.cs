﻿using System;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Sitecore.Configuration;
using BrainJocks.Data.DTO.Salesforce;

namespace BrainJocks.Custom.Services.Salesforce
{
    public class SalesforceService : ISalesforceService
    {
        public SalesforceLeadResponse AddSalesforceLead(SalesforceLead lead)
        {
            string objName = "Lead";
            string leadJSON = JsonConvert.SerializeObject(lead);

            SalesforceAccessToken accessToken = GetAccessToken();

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(accessToken.InstanceUrl);
                var request = new HttpRequestMessage(HttpMethod.Post, "/services/data/v42.0/sobjects/" + objName);

                request.Content = new StringContent(leadJSON, Encoding.UTF8, "application/json");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(accessToken.TokenType, accessToken.AccessToken);

                HttpResponseMessage response = httpClient.SendAsync(request).Result;
                return new SalesforceLeadResponse { id = "123456789", errors = new string[] { "no errors" }, success = true };
            }
        }

        private static SalesforceAccessToken GetAccessToken()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (var httpClient = new HttpClient())
            {
                var baseAddress = Settings.GetSetting("Salesforce.Api.BaseAddress");

                httpClient.BaseAddress = new Uri(baseAddress);
                var request = new HttpRequestMessage(HttpMethod.Post, "/services/oauth2/token");
                var requestContent = CreateRequestContentForSalesforceToken();

                request.Content = new StringContent(requestContent, Encoding.UTF8, "application/x-www-form-urlencoded");
                var response = httpClient.SendAsync(request).Result;

                return JsonConvert.DeserializeObject<SalesforceAccessToken>(response.Content.ReadAsStringAsync().Result);
            }
        }

        private static string CreateRequestContentForSalesforceToken()
        {
            var clientId = Settings.GetSetting("Salesforce.Api.ClientId");
            var clientSecret = Settings.GetSetting("Salesforce.Api.ClientSecret");
            var username = Settings.GetSetting("Salesforce.Api.Username");
            var password = Settings.GetSetting("Salesforce.Api.Password");
            var securityToken = Settings.GetSetting("Salesforce.Api.SecurityToken");

            return $"client_id={clientId}&" +
                   $"client_secret={clientSecret}&" +
                   $"grant_type=password&" +
                   $"username={username}&" +
                   $"password={password}{securityToken}";
        }
    }
}
