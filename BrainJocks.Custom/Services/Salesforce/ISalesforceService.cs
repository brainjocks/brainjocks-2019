﻿using BrainJocks.Data.DTO.Salesforce;

namespace BrainJocks.Custom.Services.Salesforce
{
    public interface ISalesforceService
    {
        SalesforceLeadResponse AddSalesforceLead(SalesforceLead lead);
    }
}
