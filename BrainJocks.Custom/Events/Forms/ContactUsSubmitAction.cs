﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using Sitecore.ExperienceForms.Models;
using Sitecore.ExperienceForms.Processing;
using Sitecore.ExperienceForms.Processing.Actions;
using Sitecore.Diagnostics;
using BrainJocks.Data.DTO.Salesforce;
using BrainJocks.Custom.Services.Salesforce;

namespace BrainJocks.Custom.Salesforce.Events.Forms
{
    public class ContactUsSubmitAction : SubmitActionBase<string>
    {
        private readonly ISalesforceService _salesForceService = ServiceLocator.ServiceProvider.GetService<ISalesforceService>();

        public ContactUsSubmitAction(ISubmitActionData submitActionData) : base(submitActionData)
        {
        }

        public override void ExecuteAction(FormSubmitContext formSubmitContext, string parameters)
        {
            Assert.ArgumentNotNull((object)formSubmitContext, nameof(formSubmitContext));

            if (this.TryParse(parameters, out string target))
            {
                try
                {
                    if (this.Execute(target, formSubmitContext))
                        return;
                }
                catch (ArgumentNullException ex)
                {
                }
            }
            formSubmitContext.Errors.Add(new FormActionError()
            {
                ErrorMessage = this.SubmitActionData.ErrorMessage
            });
        }

        protected override bool Execute(string data, FormSubmitContext formSubmitContext)
        {
            Assert.ArgumentNotNull(data, nameof(data));
            Assert.ArgumentNotNull(formSubmitContext, nameof(formSubmitContext));

            // Prepare the Salesforce lead model here
            var model = new SalesforceLead()
            {
                FirstName = GetValue(formSubmitContext.Fields.FirstOrDefault(f => f.Name.Equals("First Name"))),
                LastName = GetValue(formSubmitContext.Fields.FirstOrDefault(f => f.Name.Equals("Last Name"))),
                CompanyEmail = GetValue(formSubmitContext.Fields.FirstOrDefault(f => f.Name.Equals("Company Email"))),
                CompanyPhone = GetValue(formSubmitContext.Fields.FirstOrDefault(f => f.Name.Equals("Phone"))),
                CompanyName = GetValue(formSubmitContext.Fields.FirstOrDefault(f => f.Name.Equals("Company Name"))),
                HowCanWeHelpYou = GetValue(formSubmitContext.Fields.FirstOrDefault(f => f.Name.Equals("How can we help you"))),
                CampaignName = "Contact Us Form",
                LeadSource = "Web Site",
                DigitalReadinessBusinessGoalsScore = 0.0M,
                DigitalReadinessDataCollectionScore = 0.0M,
                DigitalReadinessResourcesScore = 0.0M,
                DigitalReadinessTechnicalScore = 0.0M,
                DigitalReadinessTotalScore = 0.0M
            };

            // Call the Saleforce API 
            SalesforceLeadResponse response = _salesForceService.AddSalesforceLead(model);

            return true;
        }

        private static string GetValue(object field)
        {
            return field?.GetType().GetProperty("Value")?.GetValue(field, null)?.ToString() ?? string.Empty;
        }

        protected override bool TryParse(string value, out string target)
        {
            target = string.Empty;
            return true;
        }
    }
}
