﻿using Newtonsoft.Json;

namespace BrainJocks.Data.DTO.Salesforce
{
    public class SalesforceLead
    {
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Email")]
        public string CompanyEmail { get; set; }

        [JsonProperty("Phone")]
        public string CompanyPhone { get; set; }

        [JsonProperty("Company")]
        public string CompanyName { get; set; }

        [JsonProperty("LeadSource")]
        public string LeadSource { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Campaign__c")]
        public string CampaignName { get; set; }

        [JsonProperty("ClientNeedType__c")]
        public string HowCanWeHelpYou { get; set; }

        [JsonProperty("Digital_Readiness_Business_Goals_Score__c")]
        public decimal DigitalReadinessBusinessGoalsScore { get; set; }

        [JsonProperty("Digital_Readiness_Data_Collection_Score__c")]
        public decimal DigitalReadinessDataCollectionScore { get; set; }

        [JsonProperty("Digital_Readiness_Resources_Score__c")]
        public decimal DigitalReadinessResourcesScore { get; set; }

        [JsonProperty("Digital_Readiness_Technical_Score__c")]
        public decimal DigitalReadinessTechnicalScore { get; set; }

        [JsonProperty("Digital_Readiness_Total_Score__c")]
        public decimal DigitalReadinessTotalScore { get; set; }
    }
}
