﻿using System.Collections.Generic;

namespace BrainJocks.Data.DTO.Salesforce
{
    public class SalesforceLeadResponse
    {
        public string id { get; set; }
        public bool success { get; set; }
        public string[] errors { get; set; }
    }
}
